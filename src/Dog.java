/**
 * Created by Ruben on 02/10/2014.
 */
public class Dog {

    private String name;
    private double weight;
    protected  String colour;

    private String hola;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public void printToConsole() {
        System.out.println();
        System.out.println("name: " + getName());
        System.out.println("colour: " + this.colour);
        System.out.println("weight: " + this.weight);

    }

}
